import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({providedIn: 'root'})
export class PaymentsService {
  constructor(private http: HttpClient) {}

  processPayment(token: any, amount) {
    this.http.post('http://localhost:3000/shoppingcart/checkout', {token, amount}).subscribe();
  }
}
