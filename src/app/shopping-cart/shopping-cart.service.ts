import { Injectable, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({providedIn: 'root'})
export class ShoppingCartService implements OnInit {
  productsChanged = new Subject<any[]>();

  constructor(private http: HttpClient) {}

  ngOnInit() {}

  getShoppingCartItems() {
    this.http.get<any>('http://localhost:3000/shoppingcart').subscribe(
      response => {
        // this.cartItems = response.products;
        // this.productsChanged.next([...this.cartItems]);
        this.productsChanged.next([...response.products]);
      }, error => {
        console.log('error', error);
      }
    );
  }

  addToShoppingCart(product) {
    const modifiedProduct = {
      name: product.name,
      image: product.image,
      price: product.price,
      count: 1
    };
    this.http.post<any>('http://localhost:3000/shopping-cart', modifiedProduct).subscribe(
      response => {
        console.log(response.message);
      }, error => {
        console.log(error);
      }
    );
  }

  removeFromShoppingCart(product) {
    this.http.post<{message: string}>('http://localhost:3000/', product).subscribe(
      response => {
        this.getShoppingCartItems();
      }, error => {
        console.log('Error:', error);
      }
    );
  }
  // this deletes multiple duplicate items from shopping cart
  removeAllFromShoppingCart(product) {
    this.http.put('http://localhost:3000/', product).subscribe(
      response => {
        this.getShoppingCartItems();
      }, error => {
        console.log('Error:', error);
      }
    );
  }

  getProductsChangedListener() {
    return this.productsChanged.asObservable();
  }

  // stripe checkout functions
  processPayment(token: any, amount) {
    this.http.post('http://localhost:3000/shoppingcart/checkout', {token, amount}).subscribe();
  }
}
