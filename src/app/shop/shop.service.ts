import {Injectable} from '@angular/core';
import {Recipe} from './recipes.model';

@Injectable({providedIn: 'root'})
export class ShopService {
 breadRecipes = [new Recipe('Spicy Soup',
   'http://cdn-image.myrecipes.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/1506120378/MR_0917170472.jpg?itok=KPTNrvis',
   3.00, 1), new Recipe('Good Tacos',
   'https://tmbidigitalassetsazure.blob.core.windows.net/secure/RMS/attachments/37/1200x1200/exps47811_SD153208B08_08_7b.jpg',
   2.50, 1),
   new Recipe('Gucci Pizza', 'https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg', 12.00, 1)
 ];

// breadRecipes = [new Recipe('So good', 'https://www.pritikin.com/wp/wp-content/uploads/2017/01/best-meals-for-weight-loss.jpg', 45, 1),
// new Recipe('Spicy Soup',
// 'http://cdn-image.myrecipes.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/1506120378/MR_0917170472.jpg?itok=KPTNrvis',
// 3.00, 1), new Recipe('Good Tacos',
// 'https://tmbidigitalassetsazure.blob.core.windows.net/secure/RMS/attachments/37/1200x1200/exps47811_SD153208B08_08_7b.jpg',
// 2.50, 1),
// new Recipe('Gucci Pizza', 'https://upload.wikimedia.org/wikipedia/commons/a/a3/Eq_it-na_pizza-margherita_sep2005_sml.jpg', 12.00, 1)
// ];

  getBreadRecipes() {
    return [...this.breadRecipes];
  }
}
