const express = require('express');
const bcrypt = require('bcrypt');
const router = express.Router();
const mysql = require('mysql');
const jwt = require('jsonwebtoken');


const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  // insecureAuth: true,
  password: 'sousouke99',
  // port: '3000',
  database: 'shoppingcartdb',
  multipleStatements: true
});

router.post('/signupuser', (req, res, next) => {
  bcrypt.hash(req.body.password, 10).then(
    hash => {
      const user = {
        email: req.body.email,
        password: hash
      };
      let sql = 'INSERT INTO users SET ?';
      connection.query(sql, user, (err, result) => {
        if (err) {
            console.log('err', err);
            res.status(400).json({
            message: 'Server could not process request, user was not created. Try again.',
            error: err
          });
        } else {
          res.status(201).json({
            message: 'User was created successfully',
            error: null
          });
        }
      });
    });
});

router.post('/loginuser', (req, res, next) => {
  // let sql = `SELECT * FROM users WHERE email = ${req.body.email}`;
  let sql = `SELECT * FROM users WHERE email = '${req.body.email}'`;
  connection.query(sql, (err, result) => {
    if (err) throw err;
    else if (result.length === 0) {
      res.status(401).json({
        error: 'Authentication failed, no user found with the provided email.'
      });
    } else {
      bcrypt.compare(req.body.password, result[0].password).then(
        comparisonResult => {
          if (!comparisonResult) return res.status(401).json({error: 'Authentication failed, password incorrect!'});
            const token = jwt.sign({email: req.body.email, userId: result[0].userID}, '6srT</!3;tvMxLk5', {expiresIn: '1h'});
            const decodedToken = jwt.decode(token);
            res.status(200).json({
              message: 'User authentication successful, the user is logged in.',
              token: token,
              expiresIn: 3600
            });
        }
      );
    };
  });
});

module.exports = router;
