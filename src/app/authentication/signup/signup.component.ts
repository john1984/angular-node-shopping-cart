import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  form: FormGroup;
  signupFailed: boolean;
  constructor(private autService: AuthService) { }

  ngOnInit() {
    this.signupFailed = this.autService.getisAuthenticated();

    this.form = new FormGroup({
      'email': new FormControl(null, {validators: [Validators.required, Validators.email]}),
      'password': new FormControl(null, {validators: [Validators.required, Validators.minLength(3)]})
    });
    this.autService.getsignupFailedListener().subscribe(signupFailed =>  {
      this.signupFailed = signupFailed;
    });
  }

  onSignup() {
    if (!this.form.valid) {
      console.log('form invalid');
      return;
    }
    this.autService.createUser(this.form.value.email, this.form.value.password);
  }
}
