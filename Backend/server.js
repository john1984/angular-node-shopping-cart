const mysql = require('mysql');
const bodyParser = require('body-parser');
const createError = require('http-errors');
const express = require('express');
const app = express();

//local imports
const usersRoutes = require('./routes/users-routes');
const productsRoutes = require('./routes/products-routes');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  // insecureAuth: true,
  password: 'sousouke99',
  // port: '3000',
  database: 'shoppingcartdb',
  multipleStatements: true
});

//handling mysql server connection & disconnection
connection.connect(err => {
  if (err) {
    console.log('Error connecting to MySQL \n', err);
  } else {
    console.log('Connected to MySQL!');
  }
});

app.use(bodyParser.json());

//cors error
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, PATCH, PUT, DELETE, OPTIONS"
  );
  next();
});

//routes
app.use(productsRoutes);
app.use(usersRoutes);

//catch 404 erros and forward them to error handler
app.use((req, res, next) => {
  next(createError(404));
});

//error hanlder
app.use((err, req, res, next) => {
  res.status(err.status || 500);
  res.send('404: not found');
});

app.listen(3000, () => {
  console.log('Listening for requests on port 3000!');
});
