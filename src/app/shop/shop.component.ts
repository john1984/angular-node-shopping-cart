import { Component, OnInit } from '@angular/core';
import {ShopService} from './shop.service';
import {Recipe} from './recipes.model';
import { ShoppingCartService } from '../shopping-cart/shopping-cart.service';
import { AuthService } from '../authentication/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css']
})
export class ShopComponent implements OnInit {
  isNotLoggedIn = true;
  breadRecipes: Recipe[];

  constructor(private shopService: ShopService, private shoppingCartService: ShoppingCartService,
    private authService: AuthService) { }

  ngOnInit() {
    this.breadRecipes = this.shopService.getBreadRecipes();
    // this.authService.getisNotLoggedInListener().subscribe(
    //   isNotLoggedIn => {
    //     this.isNotLoggedIn = isNotLoggedIn;
    //     console.log(this.isNotLoggedIn);
    //   }
    // );
  }

  onAddToShoppingCart(product) {
    this.shoppingCartService.addToShoppingCart(product);
  }
}
