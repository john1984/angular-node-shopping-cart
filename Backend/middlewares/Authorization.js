const jwt = require('jsonwebtoken');

const CheckAuth = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(" ")[1];
    jwt.verify(token, '6srT</!3;tvMxLk5');
    next();
  } catch (e) {
    res.status(401).json({
      message: 'Not Authorized!'
    });
  }
};

module.exports = CheckAuth;
